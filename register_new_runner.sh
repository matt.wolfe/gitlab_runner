docker-compose exec gitlab-runner-container \
    gitlab-runner register \
    --non-interactive \
    --url https://gitlab.com/ \
    --registration-token "<PROJECT TOKEN HERE>" \
    --executor docker \
    --description "<Insert your name here>" \
    --docker-image "docker:stable" \
    --docker-memory "4096m" \
    --docker-cpus "2" \
    --docker-volumes /var/run/docker.sock:/var/run/docker.sock